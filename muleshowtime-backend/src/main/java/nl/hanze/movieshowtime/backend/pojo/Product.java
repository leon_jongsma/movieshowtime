package nl.hanze.movieshowtime.backend.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Product implements Serializable
{
	private String availabilityCode;
	private String availabilityDescription;
	private String condition;
	private String ean;
	private String gpc;
	private String id;
	private String price;
	private String productId;
	private String title;
	private String year;
	
	
	
	
	@Override
	public String toString() {
		return "Product [availabilityCode=" + availabilityCode
				+ ", availabilityDescription=" + availabilityDescription
				+ ", condition=" + condition + ", ean=" + ean + ", gpc=" + gpc
				+ ", id=" + id + ", price=" + price + ", productId="
				+ productId + ", title=" + title + ", year=" + year + "]";
	}
	public String getAvailabilityCode() {
		return availabilityCode;
	}
	public void setAvailabilityCode(String availabilityCode) {
		this.availabilityCode = availabilityCode;
	}
	public String getAvailabilityDescription() {
		return availabilityDescription;
	}
	public void setAvailabilityDescription(String availabilityDescription) {
		this.availabilityDescription = availabilityDescription;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getEan() {
		return ean;
	}
	public void setEan(String ean) {
		this.ean = ean;
	}
	public String getGpc() {
		return gpc;
	}
	public void setGpc(String gpc) {
		this.gpc = gpc;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	

}

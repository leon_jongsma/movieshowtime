package nl.hanze.movieshowtime.backend.transformer;

import nl.hanze.movieshowtime.backend.pojo.Booking;
import nl.hanze.movieshowtime.backend.pojo.Product;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;

public class ProductTransformer extends org.mule.transformer.AbstractMessageTransformer 
{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException 
	{
		if (message.getPayload() instanceof Product)
		{
			Product product = (Product) message.getPayload();
			Booking booking = new Booking();
			booking.setMovie(product.getTitle());
			booking.setCinema("pathe");
			booking.setEmailaddress("leon.jongsmna@gmail.com");
			booking.setStartdate("01012015");
			booking.setTickets("25");
			return booking;
		}
		// TODO Auto-generated method stub
		return null;
	}

}

package nl.hanze.movieshowtime.backend.pojo;

/*
 * 
 * "category" : {
                                        "categoryId" : "6146",
                                        "id" : "1",
                                        "name" : "Avontuur"
                                }

 * 
 * 
 * 
 */


public class Category 
{
	private Integer categoryId;
	private Integer id;
	private String name;
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) 
	{
		this.categoryId = categoryId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	

}

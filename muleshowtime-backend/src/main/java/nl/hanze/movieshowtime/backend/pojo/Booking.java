package nl.hanze.movieshowtime.backend.pojo;

public class Booking 
{
	private String movie;
	private String cinema;
	private String startdate;
	private String tickets;
	private String emailaddress;
	public String getMovie() {
		return movie;
	}
	public void setMovie(String movie) {
		this.movie = movie;
	}
	public String getCinema() {
		return cinema;
	}
	public void setCinema(String cinema) {
		this.cinema = cinema;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getTickets() {
		return tickets;
	}
	public void setTickets(String tickets) {
		this.tickets = tickets;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	@Override
	public String toString() {
		return "Booking [movie=" + movie + ", cinema=" + cinema
				+ ", startdate=" + startdate + ", tickets=" + tickets
				+ ", emailaddress=" + emailaddress + "]";
	}
	
	
}

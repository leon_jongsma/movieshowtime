package nl.hanze.movieshowtime.booking.repository.impl;

import java.util.List;

import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import nl.hanze.movieshowtime.booking.model.Booking;
import nl.hanze.movieshowtime.booking.model.Cinema;
import nl.hanze.movieshowtime.booking.model.Movie;
import nl.hanze.movieshowtime.booking.repository.MovieShowtimeRepository;

@Named
@Transactional(readOnly=true)
public class MovieShowtimeRepositoryImpl extends AbstractRepository implements MovieShowtimeRepository 
{
	@Transactional(readOnly=false)
	public Booking createBooking(Booking booking) 
	{
		Cinema cinema = findCinema(booking.getCinema());
		Movie movie = findMovie(booking.getMovie());
		booking.setCinema(cinema);
		booking.setMovie(movie);
		em.persist(booking);		
		return booking;
	}

	

	@Transactional(readOnly=false)
	public void updateBooking(Booking booking) 
	{
		em.merge(booking);
	}

	@Transactional(readOnly=true)
	public Booking findBooking(Integer id) 
	{
		return em.find(Booking.class, id);
	}

	@Transactional(readOnly=false)
	public void deleteBooking(Integer id) 
	{
		em.remove(em.find(Booking.class, id));
	}



	

	
	
	

}

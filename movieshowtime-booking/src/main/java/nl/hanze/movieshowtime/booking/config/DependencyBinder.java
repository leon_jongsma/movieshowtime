package nl.hanze.movieshowtime.booking.config;

import nl.hanze.movieshowtime.booking.controller.BookingController;
import nl.hanze.movieshowtime.booking.controller.CinemaController;
import nl.hanze.movieshowtime.booking.controller.MovieController;
import nl.hanze.movieshowtime.booking.controller.MovieSeatController;
import nl.hanze.movieshowtime.booking.controller.impl.BookingControllerImpl;
import nl.hanze.movieshowtime.booking.controller.impl.CinemaControllerImpl;
import nl.hanze.movieshowtime.booking.controller.impl.MovieControllerImpl;
import nl.hanze.movieshowtime.booking.controller.impl.MovieSeatControllerImpl;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class DependencyBinder extends AbstractBinder 
{
	@Override
	protected void configure() 
	{
		bind(BookingControllerImpl.class).to(BookingController.class);
		bind(CinemaControllerImpl.class).to(CinemaController.class);
		bind(MovieControllerImpl.class).to(MovieController.class);
		bind(MovieSeatControllerImpl.class).to(MovieSeatController.class);
	}

}

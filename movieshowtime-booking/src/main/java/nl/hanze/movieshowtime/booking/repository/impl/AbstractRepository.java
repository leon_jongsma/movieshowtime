package nl.hanze.movieshowtime.booking.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import nl.hanze.movieshowtime.booking.model.Booking;
import nl.hanze.movieshowtime.booking.model.Cinema;
import nl.hanze.movieshowtime.booking.model.Movie;

public abstract class AbstractRepository 
{
	@PersistenceContext
	protected EntityManager em;
	
	public Cinema findCinema(Cinema cinema) 
	{
		try {
			return (Cinema) em.createNamedQuery("findCinemaFromName").setParameter("name", cinema.getName()).getSingleResult();	
		}
		catch (NoResultException e)
		{
			return saveCinema(cinema);
		}		
	}
	
	public Movie findMovie(Movie movie)
	{
		try {
			return (Movie) em.createNamedQuery("findMovieFromName").setParameter("name", movie.getName()).getSingleResult();	
		}
		catch (NoResultException e)
		{
			return saveMovie(movie);
		}		
	}
	
	public Movie saveMovie(Movie movie)
	{
		em.persist(movie);
		return findMovie(movie);
	}
	public Movie updateMovie(Movie movie)
	{
		em.merge(movie);
		return findMovie(movie);
	}
	
	
	
	public Cinema saveCinema(Cinema cinema)
	{
		em.persist(cinema);
		return findCinema(cinema);
	}
	
	public Cinema updateCinema(Cinema cinema)
	{
		em.merge(cinema);
		return findCinema(cinema);
	}
	
	public List<Booking> findBookingFromMovieName(String name) 
	{
		return em.createNamedQuery("findBookingFromMovieName").setParameter("name", name).getResultList();
	}

	public List<Booking> findBookingFromCinemaName(String name) 
	{
		return em.createNamedQuery("findBookingFromCinemaName").setParameter("name", name).getResultList();

	}

}

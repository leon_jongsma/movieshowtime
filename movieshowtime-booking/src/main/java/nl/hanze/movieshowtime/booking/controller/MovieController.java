package nl.hanze.movieshowtime.booking.controller;

import nl.hanze.movieshowtime.booking.model.Movie;


public interface MovieController 
{
	public void createMovie(Movie movie);
	public void updateMovie(Movie movie);
	public Movie findMovie(Integer id);
    public void deleteMovie(Integer id);

}

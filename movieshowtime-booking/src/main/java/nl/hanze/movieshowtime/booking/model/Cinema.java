package nl.hanze.movieshowtime.booking.model;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="mst_cinema")
@NamedQuery(name="findCinemaFromName",query="SELECT c FROM Cinema c WHERE c.name = :name")

public class Cinema extends AbstractEntity
{
	public Cinema()
	{
		
	}
	
	public Cinema(String name)
	{
		this.name = name;
	}
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}

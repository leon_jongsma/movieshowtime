package nl.hanze.movieshowtime.booking.dto;

public class BookingDto
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2964030399173092756L;
	
	private String movie;
	private String cinema;
	private String startdate;
	private Integer tickets;
	private String emailaddress;
	
	public String getMovie() {
		return movie;
	}
	public void setMovie(String movie) {
		this.movie = movie;
	}
	public String getCinema() {
		return cinema;
	}
	public void setCinema(String cinema) {
		this.cinema = cinema;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public Integer getTickets() {
		return tickets;
	}
	public void setTickets(Integer tickets) {
		this.tickets = tickets;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}

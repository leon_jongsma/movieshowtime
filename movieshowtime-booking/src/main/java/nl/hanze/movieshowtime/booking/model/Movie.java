package nl.hanze.movieshowtime.booking.model;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="mst_movie")
@NamedQuery(name="findMovieFromName",query="SELECT m FROM Movie m WHERE m.name = :name")

public class Movie extends AbstractEntity 
{
	private String name;

	public Movie()
	{
		
	}
	
	public Movie(String name) 
	{
		this.name = name;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}

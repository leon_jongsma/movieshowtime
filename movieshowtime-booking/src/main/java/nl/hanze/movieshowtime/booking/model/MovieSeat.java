package nl.hanze.movieshowtime.booking.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mst_movie_seat")
public class MovieSeat extends AbstractEntity 
{
	@ManyToOne(optional = false)
	@JoinColumn(name="cinemaId")
    private Cinema cinema;
	@ManyToOne(optional = false)
	@JoinColumn(name="movieId")
    private Movie movie;
	@Column(name="movieDate")
	@Temporal(TemporalType.DATE)
	private Date movieDate;
	@Column(name="movieTime")
	private String movieTime;
	@Column(name="seatsAvailable")
	private Integer seatsAvailable;
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Date getMovieDate() {
		return movieDate;
	}
	public void setMovieDate(Date movieDate) {
		this.movieDate = movieDate;
	}
	public String getMovieTime() {
		return movieTime;
	}
	public void setMovieTime(String movieTime) {
		this.movieTime = movieTime;
	}
	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}
	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}
	
	

}

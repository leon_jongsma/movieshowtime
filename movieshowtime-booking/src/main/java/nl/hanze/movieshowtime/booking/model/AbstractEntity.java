package nl.hanze.movieshowtime.booking.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8005838884149817248L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}


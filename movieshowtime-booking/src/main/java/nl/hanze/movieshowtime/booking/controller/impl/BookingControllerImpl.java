package nl.hanze.movieshowtime.booking.controller.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import nl.hanze.movieshowtime.booking.controller.BookingController;
import nl.hanze.movieshowtime.booking.model.Booking;
import nl.hanze.movieshowtime.booking.model.Cinema;
import nl.hanze.movieshowtime.booking.repository.MovieShowtimeRepository;


@Named
public class BookingControllerImpl implements BookingController 
{
	
	@Inject
	private MovieShowtimeRepository movieShowtimeRepository;
	
	@Override
	public Booking createBooking(Booking booking) 
	{
		return movieShowtimeRepository.createBooking(booking);
		
		
	}

	@Override
	public void updateBooking(Booking booking) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Booking findBooking(Integer id) 
	{
		return movieShowtimeRepository.findBooking(id);
	}

	@Override
	public void deleteBooking(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Booking> findAllBookings() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Booking> findBookingFromMovieName(String name) 
	{
		return movieShowtimeRepository.findBookingFromMovieName(name);

	}

	@Override
	public List<Booking> findBookingFromCinemaName(String name) 
	{
		return movieShowtimeRepository.findBookingFromCinemaName(name);

	}

	
	
}

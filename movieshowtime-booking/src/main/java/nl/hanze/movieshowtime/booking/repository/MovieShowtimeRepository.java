package nl.hanze.movieshowtime.booking.repository;

import java.util.List;

import nl.hanze.movieshowtime.booking.controller.BookingController;
import nl.hanze.movieshowtime.booking.controller.CinemaController;
import nl.hanze.movieshowtime.booking.controller.MovieController;
import nl.hanze.movieshowtime.booking.controller.MovieSeatController;
import nl.hanze.movieshowtime.booking.model.Booking;
import nl.hanze.movieshowtime.booking.model.Cinema;
import nl.hanze.movieshowtime.booking.model.Movie;

public interface MovieShowtimeRepository
{
	public Cinema findCinema(Cinema cinema);
	public Cinema saveCinema(Cinema cinema);
	public Cinema updateCinema(Cinema cinema);
	public Movie findMovie(Movie movie);
	public Movie saveMovie(Movie movie);
	public Movie updateMovie(Movie movie);
	
	public Booking createBooking(Booking booking);
	public List<Booking> findBookingFromMovieName(String name);
	public List<Booking> findBookingFromCinemaName(String name);
	public Booking findBooking(Integer id);

}

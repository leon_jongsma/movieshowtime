package nl.hanze.movieshowtime.booking.config;

import org.glassfish.jersey.server.ResourceConfig;

public class MyApplication extends ResourceConfig {

	/**
	 * Register JAX-RS application components.
	 */
	public MyApplication() 
	{
		this.register(new DependencyBinder());
	}
    
}


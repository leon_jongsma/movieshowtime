package nl.hanze.movieshowtime.booking.dto;

import java.util.List;

import nl.hanze.movieshowtime.booking.model.Booking;

public class Bookings 
{
	private List<Booking>bookings;

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}
	
	

}

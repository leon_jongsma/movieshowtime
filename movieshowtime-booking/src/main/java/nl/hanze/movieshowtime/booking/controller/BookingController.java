package nl.hanze.movieshowtime.booking.controller;

import java.util.List;

import nl.hanze.movieshowtime.booking.model.Booking;
import nl.hanze.movieshowtime.booking.model.Cinema;

public interface BookingController 
{
	//public void createBooking(Booking booking);
	public Booking createBooking(Booking booking);
	public void updateBooking(Booking booking);
	public Booking findBooking(Integer id);
    public void deleteBooking(Integer id);
    public List<Booking> findAllBookings();
    
    public List<Booking> findBookingFromMovieName(String name);
    public List<Booking> findBookingFromCinemaName(String name);
    //public Cinema findCinema(String name);
}

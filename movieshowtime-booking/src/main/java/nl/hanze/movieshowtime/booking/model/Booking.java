package nl.hanze.movieshowtime.booking.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import nl.hanze.movieshowtime.booking.dto.BookingDto;

@Entity
@Table(name="mst_booking")
@NamedQueries( {
@NamedQuery(name="findBookingFromMovieName",query="SELECT b FROM Booking b WHERE b.movie.name = :name"),
@NamedQuery(name="findBookingFromCinemaName",query="SELECT b FROM Booking b WHERE b.cinema.name = :name")
})

public class Booking extends AbstractEntity 
{
	@ManyToOne(optional = false)
	@JoinColumn(name="movieId")
    private Movie movie;
	@ManyToOne(optional = false)
	@JoinColumn(name="cinemaId")
    private Cinema cinema;
	private String startDate;
	private Integer tickets;
	private Double price;
	private Boolean purchased;
	
	public Booking()
	{
		
	}
	
	public Booking(BookingDto bookingDto) 
	{
		this.cinema = new Cinema(bookingDto.getCinema());
		this.movie = new Movie(bookingDto.getMovie());
		this.price = new Double(10.0);
		this.purchased = new Boolean(false);
		this.startDate = "01012015";
		this.tickets = bookingDto.getTickets();
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public Integer getTickets() {
		return tickets;
	}
	public void setTickets(Integer tickets) {
		this.tickets = tickets;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Boolean getPurchased() {
		return purchased;
	}
	public void setPurchased(Boolean purchased) {
		this.purchased = purchased;
	}
	
	
	
	
	
}

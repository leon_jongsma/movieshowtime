package nl.hanze.movieshowtime.booking.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import nl.hanze.movieshowtime.booking.controller.BookingController;
import nl.hanze.movieshowtime.booking.dto.BookingDto;
import nl.hanze.movieshowtime.booking.dto.Bookings;
import nl.hanze.movieshowtime.booking.model.Booking;
import nl.hanze.movieshowtime.booking.model.Cinema;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.sample.exception.ApiException;
import com.wordnik.swagger.sample.exception.NotFoundException;
import com.wordnik.swagger.sample.model.Order;
import com.wordnik.swagger.sample.model.Pet;

/**
 * @author jongsml
 * crud operations for book resource
 *
 */

@Path("/booking")
@Api(value="booking", description="Booking Api")
public class BookingResource 
{
	
	@Inject
	private BookingController bookingController;
	
	@POST
	@Path("/movie")
	@ApiOperation(value = "Create a booking")
	public Response createBooking(@ApiParam(value = "Movie that needs to get booked", required = true) BookingDto bookingDto) 
	{
		Booking booking = new Booking(bookingDto);		
		booking = bookingController.createBooking(booking);
	    return Response.ok().entity(booking).build();
	}
	
	@POST
	@Path("movie2")
	@ApiOperation(value = "Create a booking")
	public Response createBooking2(@ApiParam(value = "Movie that needs to get booked", required = true) String movieName) throws ApiException 
	{
		/*
		Booking booking = new Booking(bookingDto);		
		booking = bookingController.createBooking(booking);
		*/
	    return Response.ok().entity("SUCCESS").build();
	}
	
	@POST
	  @ApiOperation(value = "Add a new pet to the store")
	  @ApiResponses(value = { @ApiResponse(code = 405, message = "Invalid input") })
	  public Response addPet(
	      @ApiParam(value = "Pet object that needs to be added to the store", required = true) Pet pet) 
	{
	//    petData.addPet(pet);
	    return Response.ok().entity("SUCCESS").build();
	  }

	
	 @GET
	 @Path("{bookingId}")
	 @ApiOperation(value = "Find booking by id", response = Bookings.class)
	 public Response findBookingById(@ApiParam(value = "id of the booking", required = true) @PathParam("bookingId") Integer bookingId) throws ApiException 
	 {
		 Booking booking = bookingController.findBooking(bookingId);
		 if (null != booking)
		 {
			 return Response.ok().entity(booking).build();	 
		 }
		 else
		 {
			 throw new ApiException(404,"Booking not found");
		 }
	  }
	
	 @GET
	 @Path("/movie/{name}")
	 @ApiOperation(value = "Find booking by movie name", response = Bookings.class)
	 public Response getBookingByMovieName(@ApiParam(value = "name of the booked movie", required = true) @PathParam("name") String name) throws ApiException 
	 {
		 Bookings bookings = new Bookings();
		 bookings.setBookings(bookingController.findBookingFromMovieName(name));
		 if (null != bookings.getBookings())
		 {
		      return Response.ok().entity(bookings).build();
		 }
		 else
		 {
			 throw new ApiException(404,"Booking not found");
		 }
	  }
	 
}
